#include <iostream>

struct Time
{
private:
    int hr;
    int min;

public:
    void acceptTime()
    {
        printf("Enter hrs and mins = ");
        scanf("%d%d", &hr, &min);
    }
    void printTime()
    {
        printf("time = %d : %d", hr, min);
    }
};

int main()
{

    struct Time t1;
    t1.acceptTime(); // acceptTime(&t1);
    // t1.hr = 12;      // NOT OK
    t1.printTime(); // printTime(&t1);

    return 0;
}